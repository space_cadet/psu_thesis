\documentclass[11pt]{report}
\usepackage[doctor,signature]{psu-thesis}

\usepackage{amsmath,amssymb}

\usepackage{subfigure}
%
%% the following allows you to use the AAS deluxetable environment:
%\usepackage{deluxetable}
%% Use the following if you have tables that are longer than a single page::
%\usepackage{longtable}
%% Use the following if you have (non-deluxetables, i.e., longtables) that
%% you need to display landscape oriented:
%\usepackage{lscape}

\usepackage{epsfig}
\usepackage{graphicx}
\usepackage{color}
\usepackage[colorlinks=true,citecolor=blue]{hyperref}
\usepackage{wick}

% For putting frames around paragraphs
\usepackage{framed}

\numberbychapter

% Biblatex setup
% The default is the 'numerical' style.
\usepackage[backend=biber]{biblatex}
% We use the following bibliography databases:
\addbibresource{../bib_library.bib}
% End biblatex

\hypersetup{
    linkcolor=blue,          % color of internal links
    citecolor=blue,        % color of links to bibliography
    filecolor=magenta,      % color of file links
    urlcolor=cyan           % color of external links
}

\usepackage{parskip}

\raggedbottom
\setstretch{1.5}

%%% Custom commands %%%
\newcommand{\bite}{\begin{itemize}}
\newcommand{\eat}{\end{itemize}}
\newcommand{\beq}{\begin{equation}}
\newcommand{\eeq}{\end{equation}}
\newcommand{\rarrow}{\rightarrow}
\newcommand{\bra}{\langle}
\newcommand{\ket}{\rangle}
\newcommand{\beqa}{\begin{eqnarray}}
\newcommand{\eeqa}{\end{eqnarray}}
\newcommand{\barr}{\begin{array}}
\newcommand{\earr}{\end{array}}
\newcommand{\del}{\partial}
\newcommand{\de}{\mathrm{d}}
%\newcommand{\mu\nu}{{\mu\nu}}
\renewcommand{\th}{\mathrm{th}}
\newcommand{\com}[1]{\begin{itemize}\color{RED}{{#1}}\end{itemize}}
\newcommand{\C}{\mathbb{C}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\btw}[1]{\color{PURPLE}{{#1}}\color{BLACK}}
\newcommand{\cut}[1]{\color{RED}{{#1}}\color{BLACK}}

\newcommand{\mb}[1]{\mathbf{#1}}
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\mbb}[1]{\mathbb{#1}}
\newcommand{\mf}[1]{\mathfrak{#1}}

\newcommand{\vectb}[1]{\mathbf{\vec{#1}}}
\newcommand{\vecta}[1]{\overrightarrow{#1} }
\newcommand{\vect}[1]{\boldsymbol{#1}}
\newcommand{\expect}[1]{\langle #1\rangle}
\newcommand{\innerp}[2]{\langle #1 \vert #2 \rangle}
\newcommand{\fullbra}[1]{\langle #1 \vert}
\newcommand{\fullket}[1]{\vert #1 \rangle}
\newcommand{\supersc}[1]{$^{\textrm{#1}}$}
\newcommand{\subsc}[1]{$_{\textrm{#1}}$}
\newcommand{\sltwoc}{\mathfrak{sl}(2,\mathbb{C})}

\begin{document}

\title{\uppercase {The Cosmological Constant Problem\\and \\
       Many-Body Physics: An Investigation}}

\author{Deepak Vaid}

\firstreader{Stephon Alexander}{\assocprof{Physics, Haverford College} \cochair}
\secondreader{Martin Bojowald}{\assocprof{Physics} \adviserchair} 
\thirdreader{Jainendra Jain}{Erwin W. Mueller Professor of Physics}
\fourthreader{Adrian Ocneanu}{\prof{Mathematics}}
\fifthreader{Jayanth Bhanavar}{\prof{Physics} George A. and Margaret M. Downsbrough Department Head of Physics}
 
\dept{Physics}\college{Eberly College of Science}
\submitdate{April 2012}
\copyrightyear{2012}
\thesis
%\degree{Doctor of Philosophy}
\principaladviser{Stephon Alexander}
\includecopyrightpage
\includecopyrightline
%\includecommittee
%\includelistoftables
%\includelistoffigures

\listofsymbols{
\symbolentry{c}{Speed of light in a vacuum}
% \symbolentry{e}{Euler Number}
% \symbolentry{g}{Acceleration due to gravity.  According to legend, one of my 
% readers had discovered gravity because an apple fell on his head.  Of course
% this is only shown to illustrate that multi-line variable descriptions remain in
% the description column.}
% \symbolentry{M_e}{Mass of Earth}
% \symbolentry{G}{Universal gravitation constant.  Following symbols will have
% different column widths.}
% \setsymwidth{0.5in}
% \symbolentry{()'}{First derivative of a quantity.  Notice that the columns have
% moved and since the minipage environment is used, the description is wrapped to
% the new column width.  This might be used if both single character and longer
% definitions such as integral expressions, are used in the list of symbols.}
% \symbolentry{()''}{Second derivative of a quantity}
}

\dedicationtext{This thesis is dedicated to my parents.}

\abstracttext{A model for condensation of fermions in a flat Friedmann-Robertson-Walker (FRW) background is presented. It is shown that condensation can happen, via the BCS mechanism due to a four-fermion interaction which appears naturally when fermions are included in gravity. We argue that this process can form the basis for a non-perturbative resolution to the cosmological constant problem. In order to make contact with observational evidence, we show that CMB data from the WMAP3 mission can be fitted to a cosmological model with zero $\Lambda_{eff}$, provided that we live in a universe riddled with voids of the order of ~100 Mpc. For our calculations voids are approximated by LTB metrics. We argue that the correct way to model voids is based on the methods of dark matter structure formation, which are highly non-linear but are amenable to an analytic treatment.
}

\acknowltext{This work was made possible by the help and support of many people. These include:

\begin{itemize}
 \item George Peter - a teacher extraordinaire, a kind and inspiring man who not only provided moral ballast at a crucial time in my life but also encouraged me to stick to my chosen path
\item Mr. Cherian, my high school math teacher - another great educator who recognized the stifling effect schools can have on a young individuals creativity and ambition and therefore tried to nurture the same in me.
\item I thank Narendra Srivastava, Shubranghsu Sengupta the State College "gang" consisting of Vinay Kumar, Lisa Santini, Megan Broda, Whitney Polakowski, Renae DiPierre, Nathaniel Hermann, Eric Goeller, Rusty Andrews, Sean Brinza, Sarah Catalina, Darcy Worden, Aarao Cornelio and many others who were my friends in turbulent times.
\item I cannot forget the secretaries and administrative assistants at UMR and later at Penn State whose selfless efforts helped me navigate the world of forms and deadlines.
\item And finally to my parents, sisters, grandmother, uncles, cousins and all of my extended family I am eternally grateful for their love, patience and support throughout my graduate years. To them I dedicate this thesis.
\end{itemize}}
% \prefacetext{This is the preface}
% \epigraphtext{This is the Epigraph}
\frontispiece{\clearpage

\vspace*{2.0truein}

%\LARGE
\parbox{4.0truein}{
\par\noindent
Let no man ignorant of Geometry enter here\\
\hspace*{\fill}--Plato.
}
\normalsize

\vspace*{2.0truein}

%\LARGE
\parbox{4.0truein}{
\par\noindent
And I cherish more than anything else \\
\hspace*{0.5 in}the Analogies, my most trustworthy masters. \\
They know all the secrets of Nature, \\
\hspace*{0.5 in}and they ought least to be neglected in Geometry.\\
\hspace*{\fill}--Johannes Kepler
}}
\makefrontmatter

\include{chapters/intro}

\include{chapters/bcs_gravity}
%
\include{chapters/lambda_relaxation}

\include{chapters/voids_vs_lcdm}


\includebibliography{
\printbibliography
}

% \singleappendix  

\pagebreak


% Alternatively, print name centered and bold:
\centerline{\huge \bf Deepak Vaid}
\vspace{0.25in}

\begin{minipage}{0.45\linewidth}
  A-16, 1st Floor (East Facing) \\
  South Extension, Part II \\
  New Delhi - 110049 \\
  India
\end{minipage}
\begin{minipage}{0.45\linewidth}
  \begin{tabular}{ll}
    Phone: & \textbf{+91-11-65363116} (Landline)\\
     &  +91-9717275953 (Cell) \\
    Email: & \href{mailto:dvaid79@gmail.com}{\tt dvaid79@gmail.com} \\
    Homepage: & \href{http://www.phys.psu.edu/people/display/index.html?person_id=1861}{\tt Link} \\
  \end{tabular}
\end{minipage}

\section*{Personal}

\begin{itemize}
\item Citizen of India
\end{itemize}

\section*{Education}

\begin{itemize}
  \item B.S. Physics, University of Missouri at Rolla\footnote{now known as Missouri University of Science and Technology }, 2000--2003
  \item Ph.D. Physics, Pennsylvania State University, 2003--2011, (defended Nov. 2010, graduation May, 2012)
\end{itemize}


\section*{Research Experience}

\subsection*{Doctoral Research}
\begin{itemize}
\item \textbf{Independent study}, Advisor: Prof. Abhay Ashtekar, Topic: \emph{Geometric Quantum Mechanics}; Pennsylvania State University, (2004--2005)
\item \textbf{Independent study}, Advisor: Prof. Martin Bojowald; Pennsylvania State University, (2005--2006)
\item \textbf{Thesis research}, Advisor: Prof. Stephon H. S. Alexander, Pennsylvania State University, (2006--2010). Research topics covered include:
\begin{enumerate}
\item Studying the effects of LTB metric on anisotropies of the Cosmic Microwave Background using WMAP3 data
\item Applying Markov Chain Monte Carlo (MCMC) to obtain likelihood for Cosmological model fits to WMAP3 data.
\item Condensation of fermions in a cosmological setting
\item Generating Inflation from Condensates
\item Cosmological Parameter Extraction from WMAP3 data via Bayesian Analysis 
\end{enumerate}
\end{itemize}

\subsection*{Undergraduate Research}
\begin{itemize}
\item University of Missouri at Rolla, 2000--2003
\item Advisor: Prof. Donald Madison
\item Numerical Calculation of Ionization Cross-Sections of Noble Gases 
\end{itemize}

\section*{Publications}

\begin{itemize}
\item Gravity Induced Chiral Condensate Formation and the Cosmological Constant (with. S. Alexander), 2006, \href{http://www.arxiv.org/hep-th/0609066}{arXiv:hep-th/0609066}
\item A fine tuning free resolution of the cosmological constant problem (with S. Alexander), 2007, \href{http://www.arxiv.org/hep-th/0702064}{arXiv:hep-th/0702064}
\item Local Void vs Dark Energy: Confrontation with WMAP and Type Ia Supernovae (with S. Alexander, T. Biswas and A. Notari), 2008, \href{http://www.arxiv.org/abs/0712.0370}{arXiv:abs/0712.0370}
\item Embedding the Bilson-Thompson model in an LQG-like framework, 2010, \href{http://www.arxiv.org/abs/1002.1462}{arXiv:abs/1002.1462}
\item Loop Quantum Gravity for the Bewildered, (with S. Bilson-Thompson), 2011, In Progress
\item Elementary Particles as Gates for Universal Quantum Computation, 2011, In Progress
\item The Quantum Hall Effect and Black Hole Entropy, 2011, In Progress.
\item Anti-DeSitter Condensates as Black Hole Interiors, 2012, In Progress.
\end{itemize}

\section*{Talks, Seminars}
\begin{itemize}
	\item \emph{Elementary Particles as Gates for Universal Quantum Computation}, Mehta Research Institute, Allahabad, India, April 7, 2010
	\item \emph{Elementary Particles as Gates for Universal Quantum Computation}, Center for High Energy Physics, Indian Institute of Science, Bangalore, India, April 21, 2010
	\item \emph{Loop Quantum Gravity for the Bewildered}, Physics Department, University of Adelaide, Adelaide, Australia, August 19, 22 and 29, 2011
\end{itemize}

\section*{Teaching Experience}
\begin{itemize}
	\item \textit{Teaching Assistant}, Pennsylvania State University, 2003--2007
	\item \textit{Physics Tutor}, University of Missouri at Rolla, 2000--2002
\end{itemize}

\section*{Computational Skills}

\begin{itemize}
	\item \emph{Proficient in:} C++ \& Python programming, \LaTeX, Mathematica
	\item \emph{Platforms:} Microsoft Windows, Apple OS X, Ubuntu Linux
	\item \emph{Familiar with:} Matlab, SAGE
%	\item \emph{Projects:}
%	\begin{enumerate}
%		\item Numerical Computations on Clusters. 
%	\end{enumerate}
\end{itemize}

%\subsection*{Current Projects}
%
%\begin{enumerate}
%\item \textbf{Particles as topological structures in quantum gravity}: In a recent paper (\href{http://www.arxiv.org/abs/1002.1462}{arXiv.org:abs/1002.1462}) I have shown one can embed the Bilson-Thompson braid model of elementary particles in a spin-network like framework.
%\item \textbf{Tetrad Condensation}: The action for General Relativity can be cast into a simple form called the Quadratic Spinor Lagrangian. In this form, it becomes manifest that one can treat the tetrad fields as fermionic matter variables. The presence of a non-zero positive cosmological constant term provides the four-fermionic interaction term necessary for the formation of "cooper pairs" which then play the role of atoms of geometry in a theory of quantum gravity (\emph{work in progress})
%\item \textbf{deSitter Hamiltonian and Spin Systems}: The hamiltonian for gravity with non-zero $\Lambda$ (i.e. the deSitter solution) in the connection formulation can be shown to have the same form as the hamiltonian of a spin-system, when we treat tetrads as spins (\emph{work in progress})
%\item \textbf{The Computational Universe}: There exist deep connections between computational and physical law. Recent work on quantum computation and information theory has only made the question more urgent. In the above mentioned paper (\href{http://www.arxiv.org/abs/1002.1462}{arXiv.org:abs/1002.1462}) it was shown that the braid model of elementary particles can be embedded in a spin-network like framework. This combined with the observation by Lomonaco and Kauffmann (\href{http://www.arxiv.org/quant-ph/0401090}{arXiv:quant-ph/0401090}) that the elements of the braid group $B_3$ form a set of universal gates for quantum computation, lead us to a concrete theoretical model of the \emph{Computational Universe} hypothsis.  (\emph{work in progress})
%\end{enumerate}

\section*{References}

\begin{itemize}
	\item \href{http://www.adelaide.edu.au/directory/sundance.bilson-thompson}{Sundance Bilson-Thompson}, Ramsay Postdoctoral Fellow, School of Chemistry and Physics, University of Adelaide, sundance.bilson-thompson@adelaide.edu.au
	\item \href{http://www.phys.psu.edu/~jain/}{Jainendra K. Jain}, Erwin W. Mueller Professor of Physics, Pennsylvania State University, PA, USA, jain@phys.psu.edu
	\item \href{http://www.haverford.edu/faculty/salexand}{Stephon Alexander}, Associate Professor of Physics, Haverford College, Haverford, PA, USA, salexand@haverford.edu
	\item \href{http://www.phys.psu.edu/people/display/index.html?person_id=417}{Martin Bojowald}, Associate Professor of Physics, Pennsylvania State University, PA, USA, bojowald@gravity.psu.edu
\end{itemize}

% Footer
\begin{center}
  \begin{footnotesize}
    Last updated: \today \\
%     \href{\footerlink}{\texttt{\footerlink}}
  \end{footnotesize}
\end{center}


\UMIabstract{A model for condensation of fermions in a flat Friedmann-Robertson-Walker (FRW) background is presented. It is shown that condensation can happen, via the BCS mechanism due to a four-fermion interaction which appears naturally when fermions are included in gravity. We argue that this process can form the basis for a non-perturbative resolution to the cosmological constant problem. In order to make contact with observational evidence, we show that CMB data from the WMAP3 mission can be fitted to a cosmological model with zero $\Lambda_{eff}$, provided that we live in a universe riddled with voids of the order of ~100 Mpc. For our calculations voids are approximated by LTB metrics. We argue that the correct way to model voids is based on the methods of dark matter structure formation, which are highly non-linear but are amenable to an analytic treatment.}

\end{document}
